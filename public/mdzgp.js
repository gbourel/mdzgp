(function (){
  const VERSION = 'v0.0.2';

  // Couleurs
  // Route #494949
  // Herbe #52B123
  // Sable #f0b72a

  // Paramètre de la simulation
  const sim = {
    start: 0,          // timestamp depart
    duration: 0,       // durée
    speedFactor: 600  // accélération du temps (eg. 2.0 : x2)
  }

  // Piste
  const tracks = [{
    points: [[2000, 10000],   // liste des points de la piste
             [27000, 10000]],
    map: [29000, 20000],      // largeur et hauteur de la carte
    start: [2000, 10000, 0],            // position et angle du départ
    display: [0, 0],
    elt: document.getElementById('track')
  }];
  let track = null;

  // Voiture
  const car = {
    x: 0,     // position en x (m)
    y: 0,     // position en y (m)
    vx: 0,    // vitesse en x (m/s)
    vy: 0,    // vitesse en y (m/s)
    ax: 0,    // acceleration en x (m/s2)
    ay: 0,    // acceleration en y (m/s2)
    angle: 0, // angle (rad)
    elt: document.getElementById('car'),
    img: 'img/austin.png'
  }

  const canvas = document.getElementById('track_bkgd');
  const ctx = canvas.getContext('2d');

  const posChartCtx = document.getElementById('pos-chart').getContext('2d');
  const speedChartCtx = document.getElementById('speed-chart').getContext('2d');
  let posChart = null;
  let speedChart = null;

  const victory = document.getElementsByClassName('victory')[0];
  const crash = document.getElementsByClassName('crash')[0];
  const echec = document.getElementsByClassName('echec')[0];

  // convert x from m to canvas x
  function convertX(m) {
    let x = m * (track.display[0] / track.map[0]);
    return x;
  }

  // convert y from m to canvas y
  function convertY(m) {
    let y = track.display[1] - m * (track.display[1] / track.map[1]);
    return y;
  }

  // display error msg
  function error(msg) {
    let elt = document.getElementById('error');
    elt.innerText = msg;
    elt.style.display = 'inline-block';
    document.getElementById('results').style.display = 'none';
    document.getElementById('response-msg').style.display = 'none';
  }

  function setTrack() {
    track = tracks[0];
    car.x = track.start[0];
    car.y = track.start[1];
    car.angle = track.start[2];
  }

  function createTrack() {
    console.info('TODO create Track');
  }

  // Affiche la route
  function renderTrack() {
    // Fond vert (herbe)
    ctx.fillStyle = '#52B123';
    ctx.fillRect(0,0, canvas.width, canvas.height);
    // Route
    ctx.beginPath();
    ctx.setLineDash([]);
    ctx.strokeStyle = '#494949';
    ctx.lineWidth = 48;
    ctx.moveTo(convertX(track.points[0][0]),
               convertY(track.points[0][1]));
    for(let i = 1; i < track.points.length; i++) {
      ctx.lineTo(convertX(track.points[i][0]),
                 convertY(track.points[i][1]));
    }
    ctx.stroke();

    // Ligne blanche centrale
    ctx.beginPath();
    ctx.strokeStyle = '#FFFFFF';
    ctx.lineWidth = 3;
    ctx.setLineDash([30, 40]);
    ctx.moveTo(convertX(track.points[0][0]),
               convertY(track.points[0][1]));
    for(let i = 1; i < track.points.length; i++) {
      ctx.lineTo(convertX(track.points[i][0]),
                 convertY(track.points[i][1]));
    }
    ctx.stroke();
  }

  // Affiche la voiture
  function renderCar() {
    const x = convertX(car.x);
    const y = convertY(car.y);
    const angle = car.angle;

    car.elt.style.display = 'block';
    car.elt.style.transform = `translate(${x}px, ${y}px) rotate(${angle * 180 / Math.PI}deg)`;
    car.elt.style['background-image'] = `url(${car.img})`;
  }

  function computeValues() {

  }

  function moveCar() {
    let now = Date.now();
    if(!sim.lastMove) {
      sim.lastMove = now;
    } else {
      let dt = ((now - sim.lastMove) / 1000) * sim.speedFactor;
      sim.lastMove = now;
      sim.total = now - sim.start;
      car.vx += car.ax * dt;
      car.vy += car.ay * dt;
      car.x += car.vx * dt;
      car.x += car.vy * dt;

      pushData(sim.total, car.x, car.vx, car.ax);
      // console.info(`dt ${dt} Total ${sim.total / 1000}s ${Math.round(sim.total / 3600)/1000}h`);
      // console.info(`Pos. (${car.x}, ${car.y}) m`);
      // console.info(`Vit. (${car.vx}, ${car.vy}) m/s (${car.vx*3.6}, ${car.vy*3.6}) km/h `);
    }
  }

  // Affiche la simulation
  function render() {
    // Affichage de la piste
    renderTrack();
    // Affichage de la voiture
    renderCar();
    // Mise à jours des paramètres
    computeValues();
    // Déplacement de la voiture
    moveCar();

    if(sim.duration > (Date.now() - sim.start)*sim.speedFactor) {
      if(car.x > 27000 + 1000) {
        crash.style.display = "block";
        sim.lastMove = 0;
      } else {
        requestAnimationFrame(render);
      }
    } else {
      // Terminé
      sim.lastMove = 0;
      if(sim.result) {
        victory.style.display = "block";
      } else {
        echec.style.display = "block";
      }
    }
  }

  // Redimensionne l'affichage
  function resize() {
    track.display = [ track.elt.clientWidth,
                      track.elt.clientHeight ];
    canvas.width = track.elt.clientWidth;
    canvas.height = track.elt.clientHeight;
  }

  function initCharts(){
    posChart = new Chart(posChartCtx, {
      type: 'line',
      data: {
        labels: [],
        datasets: [{
          label: 'Position (m)',
          borderColor: 'rgb(255, 99, 132)',
          data: [],
          lineTension: 0.2
        }]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          align: 'end'
        },
        animation: {
          duration: 0
        },
        scales: {
          yAxes: [{
            ticks: {
              maxTicksLimit: 5
            }
          }]
        }
      }
    });
    speedChart = new Chart(speedChartCtx, {
      type: 'line',
      data: {
        labels: [],
        datasets: [{
          label: 'Vitesse (m/s)',
          borderColor: 'rgb(255, 99, 132)',
          data: [],
          lineTension: 0.2
        }]
      },
      options: {
        maintainAspectRatio: false,
        legend: {
          align: 'end'
        },
        animation: {
          duration: 0
        },
        scales: {
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  function pushData(t, pos, speed, acc) {
    // console.info('Push', t, pos, speed, acc);
    posChart.data.labels.push(t);
    posChart.data.datasets[0].data.push(pos);
    posChart.update();
    speedChart.data.labels.push(t);
    speedChart.data.datasets[0].data.push(speed);
    speedChart.update();
  }

  // Initialize
  function init() {
    document.getElementById('version').textContent = VERSION;
    setTrack(0);

    resize();

    // Affichage de la piste
    renderTrack();
    // Affichage de la voiture
    renderCar();
    // Creation des graphs
    initCharts();
  }

  function getInputValue(id) {
    return parseFloat(document.getElementById(id).value);
  }

  function simStart() {
    console.info('Simulation start');
    // Duree en h
    let duree = getInputValue("v1_duree");
    // Vitesse en km/h
    let vitesse = getInputValue("v1_vitesse");
    // Pour demo
    console.info(`Durée ${duree}, vitesse ${vitesse}`);
    sim.result = Math.abs(25 - (duree * vitesse)) < 0.01;

    duree = duree * 3600;
    vitesse = vitesse / 3.6;

    car.x = track.start[0];
    car.y = track.start[1];
    car.vx = vitesse;
    
    victory.style.display = "none";
    crash.style.display = "none";
    echec.style.display = "none";
    
    sim.start = Date.now();
    sim.duration = duree * 1000;
    
    posChart.data.labels.length = 0;
    posChart.data.datasets[0].data.length = 0;
    speedChart.data.labels.length = 0;
    speedChart.data.datasets[0].data.length = 0;
    pushData(0, car.x, car.vx, car.ax);

    requestAnimationFrame(render);
  }

  window.simStart = simStart;

  init();
  simStart();
})();